class Note{
    constructor(pos, parent){
         this.position = pos;
         this.btn = this.createBtn(parent);
     }
     createBtn(parent){
         let btn = document.createElement("BUTTON");
         console.log(btn.style.backgroundColor);
         btn.addEventListener('click', function(){
            
             if(btn.style.backgroundColor == "white"){
                 btn.style.backgroundColor = "green";
             }
             else
             {
                 btn.style.backgroundColor = "white";
             }
         });
         btn.id = "note";
         btn.innerHTML = ""; 
         parent.appendChild(btn);
         return btn;
    }
}

for (let index = 0; index < 125; index++) {
    var canvas = document.getElementById('noteCanvas');    
    let btn = new Note(index, canvas);
}